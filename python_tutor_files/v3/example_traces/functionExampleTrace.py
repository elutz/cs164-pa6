{
  "code": "def foo():\n\tx = 1\n\ty = 2\nprint \"about to call function\"\nfoo()\nprint \"done\"", 
  "trace": [
    {
      "ordered_globals": [], 
      "stdout": "", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {}, 
      "heap": {}, 
      "line": 1, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 4, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 5, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {}, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": []
        }
      ], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 1, 
      "event": "call"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {}, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": []
        }
      ], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 2, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {
            "x": 1
          }, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": [
            "x"
          ]
        }
      ], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 3, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {
            "y": 2, 
            "x": 1, 
            "__return__": null
          }, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": [
            "x", 
            "y", 
            "__return__"
          ]
        }
      ], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 3, 
      "event": "return"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 6, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "foo"
      ], 
      "stdout": "about to call function\ndone\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 6, 
      "event": "return"
    }
  ]
}
