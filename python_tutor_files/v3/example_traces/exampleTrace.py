{
  "code": "x = 1; y = 3\n\ndef foo():\n\tx = 2\n\tprint x\nprint x\nfoo()\nprint x", 
  "trace": [
    {
      "ordered_globals": [], 
      "stdout": "", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {}, 
      "heap": {}, 
      "line": 1, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x"
      ], 
      "stdout": "", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "y": 3, 
        "x": 1
      }, 
      "heap": {}, 
      "line": 3, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 6, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 7, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {}, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": []
        }
      ], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 3, 
      "event": "call"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {}, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": []
        }
      ], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 4, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {
            "x": 2
          }, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": [
            "x"
          ]
        }
      ], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 5, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n2\n", 
      "func_name": "foo", 
      "stack_to_render": [
        {
          "frame_id": 1, 
          "encoded_locals": {
            "__return__": null, 
            "x": 2
          }, 
          "is_highlighted": true, 
          "is_parent": false, 
          "func_name": "foo", 
          "is_zombie": false, 
          "parent_frame_id_list": [], 
          "unique_hash": "foo_f1", 
          "ordered_varnames": [
            "x", 
            "__return__"
          ]
        }
      ], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 5, 
      "event": "return"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n2\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 8, 
      "event": "step_line"
    }, 
    {
      "ordered_globals": [
        "y", 
        "x", 
        "foo"
      ], 
      "stdout": "1\n2\n1\n", 
      "func_name": "<module>", 
      "stack_to_render": [], 
      "globals": {
        "y": 3, 
        "x": 1, 
        "foo": [
          "REF", 
          1
        ]
      }, 
      "heap": {
        "1": [
          "FUNCTION", 
          "foo()", 
          null
        ]
      }, 
      "line": 8, 
      "event": "return"
    }
  ]
}
