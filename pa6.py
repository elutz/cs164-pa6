#Constants
deposit = True;
withdrawal = False;

#Bank class
class Bank:
    #Constructor that creates a new bank.  Default is no money, no accounts, no tellers
    def __init__(self, startReserves = 0, startTellers = [], startAccounts = {}):
        self.reserves = startReserves;
        self.tellers = startTellers;
        self.accounts = startAccounts;
        self.tellerRotation = 0;

    #Add a teller to the bank
    def addTeller(self, teller):
        self.tellers.append(teller);

    #Remove a teller to the bank
    def removeTeller(self, teller):
        self.tellers.remove(teller);

    #Add an account to a bank.  Defaults to no money in the account
    def addAccount(self, accountAmount = 0):
        id = len(self.accounts)
        self.accounts[id] = accountAmount;
        self.addToReserves(accountAmount);
        return id

    #Remove account from bank
    def removeaccount(self, accountId):
        del self.accounts[accountId];

    #Add money to the bank
    def addToReserves(self, amount):
        self.reserves += amount;

    #Remove money from bank
    def takeFromReserves(self, amount):
        self.reserves -= amount;

    #Add money to individual account
    def addToAccount(self, amount, account):
        self.accounts[account] += amount;
        self.addToReserves(amount)

    #Remove money from individual account
    def takeFromAccount(self, amount, account):
        self.accounts[account] -= amount;
        self.takeFromReserves(amount);

    #Handle requests from a customer to withdraw or deposit.  Calls corresponding function on Teller to handle logic
    def handleCustomer(self, amount, account, deposit):
        self.tellers[self.tellerRotation].handleCustomer(amount, account, deposit)
        self.tellerRotation = 0 if self.tellerRotation == len(self.tellers)-1 else self.tellerRotation + 1

    #Printout of current status from bank
    def printBank(self):
        print "Accounts"
        for ac in self.accounts:
            print ac, self.accounts[ac]
        print "Reserves"
        print self.reserves


#"Abstract" class for a Teller.
class Teller:
    def __init__(self, bank = None):
        self.bank = bank
        self.account = self.bank.addAccount()
        self.bank.addTeller(self)

    #Abstract - not implemented
    def handleCustomer(self, amount, account, deposit):
        return

#"Subclass of Teller.  Does correct transactions
class HonestTeller(Teller):
    def handleCustomer(self, amount, account, deposit):
        if (deposit):
            self.bank.addToAccount(amount, account)
        else:
            self.bank.takeFromAccount(amount, account)
        self.bank.addToAccount(10, self.account)

#"Subclass of Teller.  Does dishonest transactions to steal from bank/customer
class DishonestTeller(Teller):
    def handleCustomer(self, amount, account, deposit):
        if (deposit):
            self.bank.addToAccount(.9*amount, account)
        else:
            self.bank.takeFromAccount(1.1*amount, account)
        self.bank.addToAccount(10 + .1*amount, self.account)

#Create bank, teller, and customers
SFbank = Bank()
teller1 = HonestTeller(SFbank)
teller2 = DishonestTeller(SFbank)
cust1 = SFbank.addAccount(100)
cust2 = SFbank.addAccount(1000)
cust3 = SFbank.addAccount(100)

#Sample Transactions
SFbank.handleCustomer(10,cust1,deposit)
SFbank.printBank()
SFbank.handleCustomer(20,cust2,withdrawal)
SFbank.printBank()
SFbank.handleCustomer(90,cust2,withdrawal)
SFbank.printBank()
