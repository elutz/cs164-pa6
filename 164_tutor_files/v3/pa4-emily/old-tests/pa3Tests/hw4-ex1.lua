-- Part 1
function fibCo(a,b)
      coroutine.yield(a)
      fibCo(b, a+b)
end

function fib()
   return coroutine.wrap(function() fibCo(0,1) end)
end

-- Part 2
function takeCo(stream,n)
   if n<=0 then return null end
   coroutine.yield(stream())
   takeCo(stream, n-1)
end

function take(stream,n)
   return coroutine.wrap(function()takeCo(stream,n)end)
end

-- Part 3
function filterCo(stream, prop)
   value = stream()
   if prop(value) then
      coroutine.yield(value) 
   end
   filterCo(stream, prop)
end

function filter(stream,prop)
   return coroutine.wrap(function()filterCo(stream,prop)end)
end

-- Part 4
function mapCo(stream,f)
   coroutine.yield(f(stream()))
   mapCo(stream, f)
end
function map(stream,f)
   return coroutine.wrap(function()mapCo(stream,f)end)
end