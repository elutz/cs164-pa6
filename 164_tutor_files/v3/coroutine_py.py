def x(unused):
    return 1

def y(unused):
    return 2

z = x
w = y

z(None)
y(None)
