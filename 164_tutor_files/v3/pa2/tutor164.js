function Tutor() {
    this.state =
        {
             "ordered_globals": [], 
             "stdout": "", 
             "func_name": "<module>", 
             "stack_to_render": [], 
             "globals": {}, 
             "heap": {}, 
             "line": 0, 
             "event": "step_line"
        };
    this.trace = [];
    this.heapCount = 1;
    this.curStack = [];
    this.coStack = {};
    this.stackCount = 1;
    this.coList = [null];
    this.coParent = [];
    this.corouteCount = 1;
}

Tutor.prototype = {
    nextLine: function() {
        //if (this.getCurFrameId() == null) {
        if (false) {
            this.state["line"] += 1;
        }
        this.state["event"] = "step_line"
        this.saveState();
    },
    moveLine: function(ln) {
        this.state["line"] = ln;
    },
    throwError: function(error) {
        this.state["event"] = "exception";
        this.state["exception_msg"] = error;
        this.saveState();
    },
    print: function(output) {
        this.state["stdout"] += output + "\n";
    },
    saveState: function() {
        this.trace.push(JSON.parse(JSON.stringify(this.state)));
    },
    getStackFrame: function(id) {
        for (var i = 0; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["frame_id"] == id) {
                return this.state["stack_to_render"][i];
            }
        }
        return null;
    },
    getCurFrameId: function() {
        var i = 0;
        for (i; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["is_highlighted"]) {
                return this.state["stack_to_render"][i]["frame_id"];
            }
        }
        return null;
    },
    killFrame: function(id) {
        for (var i = 0; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["frame_id"] == id) {
                return this.state["stack_to_render"].splice(i, 1);
            }
        }
    },
    functReturn: function(val, type) {
        this.state["event"] = "return";
        var i = this.getCurFrameId()
        var cframe = this.getStackFrame(i);
        this.defObj("__return__", val, type)
        this.saveState()
        if (i == null) {
            this.saveState()
            return
        }
        console.log("cstack");
        console.log(this.curStack);
        if (this.curStack.length == 0) {
            this.state["func_name"] = "<module>";
            if (cframe["stack_num"] != 0) {
                this.yieldCo(val, type);
            }
        } else {
            var tstack = this.getStackFrame(this.curStack.pop());
            console.log("returning");
            tstack["is_highlighted"] = true;
            console.log("returning2");
            this.state["func_name"] = tstack["func_name"];
        }
        if (!cframe.is_parent){
            console.log("killing : " + i);
            /*var killed = this.state["stack_to_render"].pop(this.state["stack_to_render"].indexOf(cframe));
            console.log("killed : " + killed["frame_id"]);*/
            this.killFrame(cframe["frame_id"]);
        } else {
            cframe["is_highlighted"] = false;
            cframe.is_zombie = true;
            cframe["unique_hash"] += "_z";
        }
    },
    defVal: function(input) {
        if (typeof(input) != 'undefined' && input != null && typeof(input) == typeof({})) {
            var tmp;
            if ("%%%__tid" in input) {
                tmp = input["%%%__tid"];
            } else {
                tmp = this.heapCount++;
                input["%%%__tid"] = tmp;
                this.state["heap"][tmp.toString()] = ["DICT"];
            }
            return ["REF", tmp];
        } else {
            return input;
        }
    },
    defObj: function(name, val, type) {
        name = this.cleanName(name);
        var cframeId = this.getCurFrameId();
        if (cframeId == null) {
            this.state["ordered_globals"].push(name);
            this.state["globals"][name] = this.evalInput(val, type, name);
        } else {
            var tframe = this.getStackFrame(cframeId);
            tframe["ordered_varnames"].push(name);
            tframe["encoded_locals"][name] = this.evalInput(val, type, name);
        }
    },
    getHeapVal: function(ref) {
        return this.state["heap"][ref[1].toString()];
    },
    evalInput: function(val, type, name) {
        if (type == 1 && (typeof(val["%%%__tid"]) == 'undefined')) {
            return this.createFunc(val, name);
        } else if (type == 2 && (typeof(val["%%%__tid"]) == 'undefined')) {
            return this.createCo(val);
        } else {
            return this.defVal(val);
        }
    },
    putValInDict: function(dict, key, inVal, type) {
        var dval = this.evalInput(dict, false, "");
        var heapDict = this.getHeapVal(dval);
        var val = this.evalInput(inVal, type, key);
        var i = 1;
        for (i; i < heapDict.length; i++) {
            if (heapDict[i] == key) {
                heapDict[i][1] = val;
                return;
            }
        }
        heapDict.push([key, val]);
    },
    overwriteVar: function(varName, inVal, type) {
        varName = this.cleanName(varName);
        var val = this.evalInput(inVal, type, varName);
        var tFrame = this.getStackFrame(this.getCurFrameId());
        if (tFrame != null) {
            if (tFrame["ordered_varnames"].indexOf(varName) != -1) {
                return tFrame["encoded_locals"][varName] = val;
            }
            var i = 0;
            var found = false;
            var ttFrame;
            for (i; i < tFrame["parent_frame_id_list"].length; i++) {
                ttFrame = this.getStackFrame(tFrame["parent_frame_id_list"][i]);
                if (ttFrame["ordered_varnames"].indexOf(varName) != -1) {
                    return ttFrame["encoded_locals"][varName] = val;
                }
            }
        }
        if (this.state["ordered_globals"].indexOf(varName) != -1) {
            return this.state["globals"][varName] = val;
        }
        console.log(this.state);
        throw "Variable " + varName + " can't be found";
    },
    createFunc: function(val, name) {
        if ("%%%__tid" in val) {
            return ["REF", val["%%%__tid"]];
        }
        var cframeId = this.getCurFrameId();
        if (cframeId != null) {
            var cframe = this.getStackFrame(cframeId);
            cframe["is_parent"] = true;
            cframe["unique_hash"] += "_p";
        }
        tmp = this.heapCount++;
        val["%%%__tid"] = tmp;
        if (typeof(val["args"]) == 'undefined') {
            console.log(val)
        }
        var sig = name + '(' + val["args"].join(", ") + ')';
        this.state["heap"][tmp.toString()] = ["FUNCTION", sig, cframeId, this.state["line"]];
        return ["REF", tmp];
    },
    callFunc: function(val, arg_vals, stack_num) {
        if (typeof(val) == 'undefined') {
            return
        }
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
            this.curStack.push(cframeId);
        } else {
            cframe = {}
            cframe["stack_num"] = 0;
        }
        var isLambda = !("%%%__tid" in val);
        var id = this.stackCount++;
        var name = (isLambda ? "lambda" : this.state["heap"][val["%%%__tid"].toString()][1]).split('(')[0];
        var newStack = 
        {
           "frame_id": id, 
           "encoded_locals": {}, 
           "is_highlighted": true, 
           "is_parent": false, 
           "func_name": name, 
           "is_zombie": false, 
           "parent_frame_id_list": [], 
           "unique_hash": name + "_f" + id, 
           "ordered_varnames": [],
           "stack_num": typeof(stack_num) == 'undefined' ? cframe["stack_num"] : stack_num
        }
        console.log(id);
        if (!isLambda) {
            var parentId = this.state["heap"][val["%%%__tid"].toString()][2];
            if (parentId != null) {
                var pStack = this.getStackFrame(parentId);
                newStack["parent_frame_id_list"].push(parentId);
                newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(pStack["parent_frame_id_list"]);
            }
        } else if (cframeId != null) {
            newStack["parent_frame_id_list"].push(cframeId);
            newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(cframe["parent_frame_id_list"]);
        }
        this.state["stack_to_render"].push(newStack);
        var arglength = arg_vals.length > val["args"].length ? val["args"].length : arg_vals.length;
        for (var i = 0; i < arglength; i++) {
            this.defObj(val["args"][i], arg_vals[i][0], arg_vals[i][1]);
        }
        if (!isLambda) {
            this.moveLine(this.state["heap"][val["%%%__tid"].toString()][3]);
        }
        this.state["event"] = "call";
        //this.saveState();
    },
    tcallFunc: function(val, arg_vals, stack_num) {
        console.log("tcall");
        console.log(val);
        if (typeof(val) == 'undefined') {
            return
        }
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
            if (!cframe.is_parent){
                this.killFrame(cframeId);
            } else {
                cframe["is_highlighted"] = false;
                cframe.is_zombie = true;
                cframe["unique_hash"] += "_z";
            }
        } else {
            cframe = {}
            cframe["stack_num"] = 0;
        }
        var isLambda = !("%%%__tid" in val);
        var id = this.stackCount++;
        var name = (isLambda ? "lambda" : this.state["heap"][val["%%%__tid"].toString()][1]).split('(')[0];
        var newStack = 
        {
           "frame_id": id, 
           "encoded_locals": {}, 
           "is_highlighted": true, 
           "is_parent": false, 
           "func_name": name, 
           "is_zombie": false, 
           "parent_frame_id_list": [], 
           "unique_hash": name + "_f" + id, 
           "ordered_varnames": [],
           "stack_num": typeof(stack_num) == 'undefined' ? cframe["stack_num"] : stack_num
        }
        console.log(id);
        if (!isLambda) {
            var parentId = this.state["heap"][val["%%%__tid"].toString()][2];
            if (parentId != null) {
                var pStack = this.getStackFrame(parentId);
                newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(pStack["parent_frame_id_list"]);
                console.log("stacks on stacks");
                console.log(pStack["parent_frame_id_list"]);
                console.log(newStack["parent_frame_id_list"]);
            }
        } else if (cframeId != null) {
            newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(cframe["parent_frame_id_list"]);
            console.log("stacks on stacks");
            console.log(cframe["parent_frame_id_list"]);
            console.log(newStack["parent_frame_id_list"]);
        }
        this.state["stack_to_render"].push(newStack);
        var arglength = arg_vals.length > val["args"].length ? val["args"].length : arg_vals.length;
        for (var i = 0; i < arglength; i++) {
            this.defObj(val["args"][i], arg_vals[i][0], arg_vals[i][1]);
        }
        if (!isLambda) {
            this.moveLine(this.state["heap"][val["%%%__tid"].toString()][3]);
        }
        this.state["event"] = "call";
    },
    cleanName: function(name) {
        var tname = name.replace('%', '_-');
        return tname.replace('#','--');
    },
    resetTrace: function() {
        this.trace = [];
        this.moveLine(0);
        this.state["stdout"] = "";
    },
    createCo: function(val) {
        var conum = this.corouteCount++;
        if (!(["%%%__tid"] in val.fn)) {
            this.createFunc(val.fn,"Coroutine"+conum);
            console.log("lamco");
        }
        var tmp = this.heapCount++;
        this.coList.push(null);
        val["%%%__tid"] = tmp;
        this.state["heap"][tmp.toString()] = ["COROUTINE", this.state["heap"][val.fn["%%%__tid"]][1],conum];
        return ["REF", tmp];
    },
    resumeCo: function(coval, inVal, type) {
        console.log("resuming");
        if (!(["%%%__tid"] in coval)) {
            this.createCo(coval, "tempCoroutine");
        }
        var val = this.evalInput(inVal, type);
        var coroute = this.state["heap"][coval["%%%__tid"]];
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
        }  else {
            cframe = {};
            cframe["stack_num"] = 0;
            console.log("problems be hey");
        }
        console.log("res-cframe");
        console.log(cframe);
        this.coStack[cframe["stack_num"]] = this.curStack;
        this.coList[cframe["stack_num"]] = cframeId;
        console.log("res:cframe");
        console.log(cframe);
        console.log(this.state);
        console.log(this.coStack[coroute[2]]);
        console.log(this.coList[coroute[2]]);
        console.log(this.coParent[coroute[2]]);
        if (this.coList[coroute[2]] == null) {
            this.curStack = [];
            this.callFunc(coval.fn,[[inVal,type]], coroute[2]);
        } else {
            this.curStack = this.coStack[coroute[2]];
            var nframe = this.getStackFrame(this.coList[coroute[2]]);
            nframe["is_highlighted"] = true;
        }
        this.coParent[coroute[2]] = cframe["stack_num"];
    },
    yieldCo: function(inVal, type) {
        var cframeId = this.getCurFrameId();
        var cframe = this.getStackFrame(cframeId);
        console.log("yielding");
        console.log(cframeId);
        cframe["is_highlighted"] = false;
        var val = this.evalInput(inVal, type);
        var retCo = this.coParent[cframe["stack_num"]];
        console.log("retCo");
        console.log(retCo);
        var retId = this.coList[retCo];
        this.coStack[cframe["stack_num"]] = this.curStack;
        this.curStack = this.coStack[retCo];
        var retFrame;
        if (retId != null) {
            retFrame = this.getStackFrame(retId);
            retFrame["is_highlighted"] = true;
        } else {
            retFrame = {};
            retFrame["stack_num"] = 0;
        }
        this.coList[cframe["stack_num"]] = cframeId;
        console.log(this.curStack);
        console.log(this.coStack);
        console.log(this.coList);
    }
}

var Tutor164 = Tutor;

if (typeof(module) !== 'undefined') {
  module.exports = {
    'Tutor164': Tutor164
  };
}
        


    


