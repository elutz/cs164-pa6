function Tutor() {
    this.state =
        {
             "ordered_globals": [], 
             "stdout": "", 
             "func_name": "<module>", 
             "stack_to_render": [], 
             "globals": {}, 
             "heap": {}, 
             "line": 0, 
             "event": "step_line"
        };
    this.trace = [];
    this.heapCount = 1;
    this.curStack = [];
    this.coStack = {};
    this.stackCount = 1;
    this.coList = [null];
    this.coParent = [];
    this.corouteCount = 1;
}
// val is always just the object you want to store or call
// name is always a string
// type 2 for coroutine, 1 for closure(lambda), and 0 for anything else
Tutor.prototype = {
    //Call every time a new line is looked at
    //Same as save state, but resets line event
    nextLine: function() {
        this.state["event"] = "step_line"
        this.saveState();
    },
    //Deprecated don't call on anything greater than 1
    moveLine: function(ln) {
        this.state["line"] = ln;
    },
    //error - string message of the error
    //call inside catch
    throwError: function(error) {
        this.state["event"] = "exception";
        this.state["exception_msg"] = error;
        this.saveState();
    },
    // output - string to be printed
    // call everytime you would log to the output
    print: function(output) {
        this.state["stdout"] += output + "\n";
    },
    //Internal use only
    //saves the program state to the trace
    saveState: function() {
        this.trace.push(JSON.parse(JSON.stringify(this.state)));
    },
    //Internal use only
    //get a stack frame with id from the stack_to_render
    getStackFrame: function(id) {
        for (var i = 0; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["frame_id"] == id) {
                return this.state["stack_to_render"][i];
            }
        }
        return null;
    },
    //Internal use only
    //gets the current stack frame id
    getCurFrameId: function() {
        var i = 0;
        for (i; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["is_highlighted"]) {
                return this.state["stack_to_render"][i]["frame_id"];
            }
        }
        return null;
    },
    //Internal use only
    //removes frame with id from the stack
    killFrame: function(id) {
        for (var i = 0; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["frame_id"] == id) {
                return this.state["stack_to_render"].splice(i, 1);
            }
        }
    },
    //Call when returning from function
    //standard inputs
    functReturn: function(val, type) {
        this.state["event"] = "return";
        var i = this.getCurFrameId()
        var cframe = this.getStackFrame(i);
        this.defObj("__return__", val, type)
        this.saveState()
        if (i == null) {
            this.saveState()
            return
        }
        if (this.curStack.length == 0) {
            this.state["func_name"] = "<module>";
            if (cframe["stack_num"] != 0) {
                this.yieldCo(val, type);
            }
        } else {
            var tstack = this.getStackFrame(this.curStack.pop());
            tstack["is_highlighted"] = true;
            this.state["func_name"] = tstack["func_name"];
        }
        if (!cframe.is_parent){
            this.killFrame(cframe["frame_id"]);
        } else {
            cframe["is_highlighted"] = false;
            cframe.is_zombie = true;
            cframe["unique_hash"] += "_z";
        }
    },
    //Internal only
    //handles references to already seen objects
    defVal: function(input) {
        if (typeof(input) != 'undefined' && input != null && typeof(input) == typeof({})) {
            var tmp;
            if ("%%%__tid" in input) {
                tmp = input["%%%__tid"];
            } else {
                tmp = this.heapCount++;
                input["%%%__tid"] = tmp;
                this.state["heap"][tmp.toString()] = ["DICT"];
            }
            return ["REF", tmp];
        } else {
            return input;
        }
    },
    //Call when defining variables
    //standard inputs
    defObj: function(name, val, type) {
        name = this.cleanName(name);
        var cframeId = this.getCurFrameId();
        if (cframeId == null) {
            this.state["ordered_globals"].push(name);
            this.state["globals"][name] = this.evalInput(val, type, name);
        } else {
            var tframe = this.getStackFrame(cframeId);
            tframe["ordered_varnames"].push(name);
            tframe["encoded_locals"][name] = this.evalInput(val, type, name);
        }
    },
    //Internal
    //uses a reference to acces the heap
    getHeapVal: function(ref) {
        return this.state["heap"][ref[1].toString()];
    },
    //Internal
    //evaluates type to know which function to call to create the value (or not if not necessary)
    evalInput: function(val, type, name) {
        if (type == 1 && (typeof(val["%%%__tid"]) == 'undefined')) {
            return this.createFunc(val, name);
        } else if (type == 2 && (typeof(val["%%%__tid"]) == 'undefined')) {
            return this.createCo(val);
        } else {
            return this.defVal(val);
        }
    },
    //Call when doing dictionary key, value, assignments
    //dict is the actual dict, key is the key, inVal is value to be stored, and type is as usual
    putValInDict: function(dict, key, inVal, type) {
        var dval = this.evalInput(dict, false, "");
        var heapDict = this.getHeapVal(dval);
        var val = this.evalInput(inVal, type, key);
        var i = 1;
        for (i; i < heapDict.length; i++) {
            if (heapDict[i] == key) {
                heapDict[i][1] = val;
                return;
            }
        }
        heapDict.push([key, val]);
    },
    //Call when doing environment updates
    //varName instead of name, and inVal instead of val, but standard typing
    overwriteVar: function(varName, inVal, type) {
        varName = this.cleanName(varName);
        var val = this.evalInput(inVal, type, varName);
        var tFrame = this.getStackFrame(this.getCurFrameId());
        if (tFrame != null) {
            if (tFrame["ordered_varnames"].indexOf(varName) != -1) {
                return tFrame["encoded_locals"][varName] = val;
            }
            var i = 0;
            var found = false;
            var ttFrame;
            for (i; i < tFrame["parent_frame_id_list"].length; i++) {
                ttFrame = this.getStackFrame(tFrame["parent_frame_id_list"][i]);
                if (ttFrame["ordered_varnames"].indexOf(varName) != -1) {
                    return ttFrame["encoded_locals"][varName] = val;
                }
            }
        }
        if (this.state["ordered_globals"].indexOf(varName) != -1) {
            return this.state["globals"][varName] = val;
        }
        throw "Variable " + varName + " can't be found";
    },
    //Internal Only
    //create functions, update current frame to make it a parent
    //functions must contain an "args" key that is a list of arguments names
    createFunc: function(val, name) {
        if ("%%%__tid" in val) {
            return ["REF", val["%%%__tid"]];
        }
        var cframeId = this.getCurFrameId();
        if (cframeId != null) {
            var cframe = this.getStackFrame(cframeId);
            cframe["is_parent"] = true;
            cframe["unique_hash"] += "_p";
        }
        tmp = this.heapCount++;
        val["%%%__tid"] = tmp;
        if (typeof(val["args"]) == 'undefined') {
        }
        var sig = name + '(' + val["args"].join(", ") + ')';
        this.state["heap"][tmp.toString()] = ["FUNCTION", sig, cframeId, this.state["line"]];
        return ["REF", tmp];
    },
    //Call when calling functions
    //val is the function
    //arg_val is a list of  [val, type] list of the arguments
    //don't include a stack_num
    //for internal use only
    callFunc: function(val, arg_vals, stack_num) {
        if (typeof(val) == 'undefined') {
            return
        }
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
            this.curStack.push(cframeId);
        } else {
            cframe = {}
            cframe["stack_num"] = 0;
        }
        var isLambda = !("%%%__tid" in val);
        var id = this.stackCount++;
        var name = (isLambda ? "lambda" : this.state["heap"][val["%%%__tid"].toString()][1]).split('(')[0];
        var newStack = 
        {
           "frame_id": id, 
           "encoded_locals": {}, 
           "is_highlighted": true, 
           "is_parent": false, 
           "func_name": name, 
           "is_zombie": false, 
           "parent_frame_id_list": [], 
           "unique_hash": name + "_f" + id, 
           "ordered_varnames": [],
           "stack_num": typeof(stack_num) == 'undefined' ? cframe["stack_num"] : stack_num
        }
        if (!isLambda) {
            var parentId = this.state["heap"][val["%%%__tid"].toString()][2];
            if (parentId != null) {
                var pStack = this.getStackFrame(parentId);
                newStack["parent_frame_id_list"].push(parentId);
                newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(pStack["parent_frame_id_list"]);
            }
        } else if (cframeId != null) {
            newStack["parent_frame_id_list"].push(cframeId);
            newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(cframe["parent_frame_id_list"]);
        }
        this.state["stack_to_render"].push(newStack);
        var arglength = arg_vals.length > val["args"].length ? val["args"].length : arg_vals.length;
        for (var i = 0; i < arglength; i++) {
            this.defObj(val["args"][i], arg_vals[i][0], arg_vals[i][1]);
        }
        if (!isLambda) {
            this.moveLine(this.state["heap"][val["%%%__tid"].toString()][3]);
        }
        this.state["event"] = "call";
        //this.saveState();
    },
    //Not fully functional,
    //breaks coroutines
    //don't call
    //would fix the extra stacks hanging around is tailCall optimization is turned on.
    tcallFunc: function(val, arg_vals, stack_num) {
        if (typeof(val) == 'undefined') {
            return
        }
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
            if (!cframe.is_parent){
                this.killFrame(cframeId);
            } else {
                cframe["is_highlighted"] = false;
                cframe.is_zombie = true;
                cframe["unique_hash"] += "_z";
            }
        } else {
            cframe = {}
            cframe["stack_num"] = 0;
        }
        var isLambda = !("%%%__tid" in val);
        var id = this.stackCount++;
        var name = (isLambda ? "lambda" : this.state["heap"][val["%%%__tid"].toString()][1]).split('(')[0];
        var newStack = 
        {
           "frame_id": id, 
           "encoded_locals": {}, 
           "is_highlighted": true, 
           "is_parent": false, 
           "func_name": name, 
           "is_zombie": false, 
           "parent_frame_id_list": [], 
           "unique_hash": name + "_f" + id, 
           "ordered_varnames": [],
           "stack_num": typeof(stack_num) == 'undefined' ? cframe["stack_num"] : stack_num
        }
        if (!isLambda) {
            var parentId = this.state["heap"][val["%%%__tid"].toString()][2];
            if (parentId != null) {
                var pStack = this.getStackFrame(parentId);
                newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(pStack["parent_frame_id_list"]);
            }
        } else if (cframeId != null) {
            newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(cframe["parent_frame_id_list"]);
        }
        this.state["stack_to_render"].push(newStack);
        var arglength = arg_vals.length > val["args"].length ? val["args"].length : arg_vals.length;
        for (var i = 0; i < arglength; i++) {
            this.defObj(val["args"][i], arg_vals[i][0], arg_vals[i][1]);
        }
        if (!isLambda) {
            this.moveLine(this.state["heap"][val["%%%__tid"].toString()][3]);
        }
        this.state["event"] = "call";
    },
    //Internal only, removed charactes that could cause problems for the visualizer
    cleanName: function(name) {
        var tname = name.replace('%', '_-');
        return tname.replace('#','--');
    },
    // Call this at the beginning of desugarOne, or the for loop in desugarAll
    // removes old traces, and the traces of preloaded code (object.164 ...)
    resetTrace: function() {
        this.trace = [];
        this.moveLine(0);
        this.state["stdout"] = "";
    },
    //Call in coroutine node
    //Create a coroutine,
    //val is just the coroutine object
    //val must have "fn" key that links to a closure
    createCo: function(val) {
        var conum = this.corouteCount++;
        if (!(["%%%__tid"] in val.fn)) {
            this.createFunc(val.fn,"Coroutine"+conum);
        }
        var tmp = this.heapCount++;
        this.coList.push(null);
        val["%%%__tid"] = tmp;
        this.state["heap"][tmp.toString()] = ["COROUTINE", this.state["heap"][val.fn["%%%__tid"]][1],conum];
        return ["REF", tmp];
    },
    //Call in resume node
    //resumes a coroutine
    //coval is a coroutine object
    //inVal and type are standard
    resumeCo: function(coval, inVal, type) {
        if (!(["%%%__tid"] in coval)) {
            this.createCo(coval, "tempCoroutine");
        }
        var val = this.evalInput(inVal, type);
        var coroute = this.state["heap"][coval["%%%__tid"]];
        var cframeId = this.getCurFrameId();
        var cframe;
        if (cframeId != null) {
            cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
        }  else {
            cframe = {};
            cframe["stack_num"] = 0;
        }
        this.coStack[cframe["stack_num"]] = this.curStack;
        this.coList[cframe["stack_num"]] = cframeId;
        if (this.coList[coroute[2]] == null) {
            this.curStack = [];
            this.callFunc(coval.fn,[[inVal,type]], coroute[2]);
        } else {
            this.curStack = this.coStack[coroute[2]];
            var nframe = this.getStackFrame(this.coList[coroute[2]]);
            nframe["is_highlighted"] = true;
        }
        this.coParent[coroute[2]] = cframe["stack_num"];
    },
    //yields from a coroutine
    //inVal and type are standard
    //call in yield node
    yieldCo: function(inVal, type) {
        var cframeId = this.getCurFrameId();
        var cframe = this.getStackFrame(cframeId);
        cframe["is_highlighted"] = false;
        var val = this.evalInput(inVal, type);
        var retCo = this.coParent[cframe["stack_num"]];
        var retId = this.coList[retCo];
        this.coStack[cframe["stack_num"]] = this.curStack;
        this.curStack = this.coStack[retCo];
        var retFrame;
        if (retId != null) {
            retFrame = this.getStackFrame(retId);
            retFrame["is_highlighted"] = true;
        } else {
            retFrame = {};
            retFrame["stack_num"] = 0;
        }
        this.coList[cframe["stack_num"]] = cframeId;
    }
}
//Call __blank__ = new Tutor164() to create your tutor164 object
var Tutor164 = Tutor;

if (typeof(module) !== 'undefined') {
  module.exports = {
    'Tutor164': Tutor164
  };
}
        


    


