if (typeof(module) !== 'undefined') {
  var ExecError = require('./errors.js').ExecError;
}

// TODO: Use Table to implement dictionaries, lists, and objects
// Complete all the requisite methods below
function Table() {
}

Table.prototype.put = function(key, value) {
};

Table.prototype.has_key = function(key) {
};

Table.prototype.get = function(key) {
};

Table.prototype.toString = function() {
};

Table.prototype.get_length = function() {
};

if (typeof(module) !== 'undefined') {
  module.exports = {
    Table: Table
  };
}
