'use strict';

if (typeof(module) !== 'undefined') {
  var ExecError = require('./errors.js').ExecError;
  var desugarAST = require('./desugar.js').desugarAST;
  var Tutor164 = require('./tutor164.js').Tutor164;
  var env = require('./environment.js');
  var Table = require('./table.js').Table;
  var envRoot = env.root;
  var envExtend = env.extend;
  var envBind = env.bind;
  var envUpdate = env.update;
  var envLookup = env.lookup;
}
var tutor164 = new Tutor164();

var interpret = function(asts, log, err) {
  // PA4: Bytecode interpreter.  Motivation: stackful interpreter
  // cannot implement coroutines.
  
  function compileToBytecode(ast) {
    // TODO step 2: Complete this function, which takes an AST as input
    // and produces bytecode as its output
	  
    // This helper function generates a unique register name
    function uniquegen() {
      return '#btc-reg-' + uniquegen.counter++;
    }
    uniquegen.counter = 1;

    function btcnode(node, target, btc) {
      switch (node.type) {
        //Statements
        case "asgn":
          //envUpdate(env, node.name.name, evalExpression(node.value, env));
          //return null;
          var reg2 = uniquegen();
          btcnode(node.value, reg2, btc);
          btc.push({'type': 'asgn', 'name': node.name.name, 'value': reg2, 'target': target});
          break;
        case "def":
          //envBind(env, node.name.name, evalExpression(node.value, env));
          //return null;
          var reg2 = uniquegen();
          btcnode(node.value, reg2, btc);
          btc.push({'type': 'def', 'name': node.name.name, 'value': reg2, 'target': target});
          break;
        case "print":
          // var toPrint = evalExpression(node.value, env)
          // log(makeString(toPrint));
          // return null;
          var reg1 = uniquegen();
          btcnode(node.value, reg1, btc);
          btc.push({'type': 'print', 'value': reg1, 'target': target});
          break;
        case "error":
          // throw new ExecError(evalExpression(node.message, env));
          var reg1 = uniquegen();
          btcnode(node.message, reg1, btc);
          btc.push({'type': 'error', 'message': reg1, 'target': target});
          break;
        case "exp":
          // return evalExpression(node.body, env);
          btcnode(node.body, target, btc);
          break;
        case "put":
          // var dict = evalExpression(node.dict, env);
          // var field = evalExpression(node.field, env);
          // var value = evalExpression(node.value, env);
          // if(typeof(dict) != typeof({})){
          //   throw new ExecError("Trying to put into non-table.");
          // }
          // dict[padKey(field)] = value
          // break;
          var reg1 = uniquegen();
          btcnode(node.dict, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.field, reg2, btc);
          var reg3 = uniquegen();
          btcnode(node.value, reg3, btc);
          btc.push({'type': 'put', 'dict': reg1, 'field': reg2, 'value': reg3, 'target': target});
          break;
        //Expressions
        case '+':
          //return evalExpression(node.operand1, env) + evalExpression(node.operand2, env);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '+', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '-':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to - must be numbers.');
          // }
          // return Math.floor(operandOne - operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '-', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '*':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to * must be numbers.');
          // }
          // return Math.floor(operandOne * operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '*', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '/':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to / must be numbers.');
          // }
          // if (operandTwo == 0){
          //   throw new ExecError('Division by zero');
          // }
          // return Math.floor(operandOne / operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '/', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '==':
          // return makeProperBoolean(evalExpression(node.operand1, env) == evalExpression(node.operand2, env));
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '==', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '!=':
          // return makeProperBoolean(evalExpression(node.operand1, env) != evalExpression(node.operand2, env));
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '!=', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '<=':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to <= must be numbers.');
          // }
          // return makeProperBoolean(operandOne <= operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '<=', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '>=':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to >= must be numbers.');
          // }
          // return makeProperBoolean(operandOne >= operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '>=', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '<':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to < must be numbers.');
          // }
          // return makeProperBoolean(operandOne < operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '<', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case '>':
          // var operandOne = evalExpression(node.operand1, env);
          // var operandTwo = evalExpression(node.operand2, env);
          // if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          //   throw new ExecError('Operands to > must be numbers.');
          // }
          // return makeProperBoolean(operandOne > operandTwo);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': '>', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case "id":
          // return envLookup(env, node.name);
          btc.push({'type': 'id', 'name': node.name, 'target': target});
          break;
        case "int-lit":
          // return node.value;
          btc.push({'type': 'int-lit', 'value': node.value, 'target': target});
          break;
        case "string-lit":
          // return node.value;
          btc.push({'type': 'string-lit', 'value': node.value, 'target': target});
          break;
        case "null":
          //return null;
          btc.push({'type': 'null', 'target': target});
          break;
        case "ite":
          // var cond = evalExpression(node.condition, env);
          // var ct = evalExpression(node.true,  env);
          // var cf = evalExpression(node.false, env);
          // if (cond == null) {
          //   cond = false;
          // }
          // if ((typeof cond != 'boolean') && (typeof cond != 'number')) {
          //   throw new ExecError('Condition not a boolean');
          // }
          // return cond ? ct : cf;
          var reg1 = uniquegen();
          btcnode(node.condition, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.true, reg2, btc);
          var reg3 = uniquegen();
          btcnode(node.false, reg3, btc);
          btc.push({'type': 'ite', 'condition': reg1, 'true': reg2, 'false': reg3, 'target': target});
          break;
        case "lambda":
          // return makeClosure(node.arguments, node.body, env);
          var btcBody = btcblock(node.body);
          btc.push({'type': 'lambda', 'arguments': node.arguments, 'body': btcBody, 'target': target});
          break;
        case "empty-dict-lit":
          //return {};
          btc.push({'type': 'empty-dict-lit', 'target': target});
          break;
        case "call":
          // var fn = evalExpression(node.function, env);
          // if (fn != null && fn.type && fn.type === 'closure') {
          //   var evaluatedArguments = [];
          //   for (var i = 0; i < node.arguments.length; i++) {
          //     evaluatedArguments[i] = evalExpression(node.arguments[i], env);
          //   }
          //   if (fn.names.length != node.arguments.length){
          //     throw new ExecError('Wrong number of arguments');
          //   }
          //   var newEnv = envExtend(fn.env);
          //   for (var i = 0; i < node.arguments.length; i++) {
          //     envBind(newEnv, fn.names[i].name, evaluatedArguments[i]);
          //   }
          //   return evalBlock(fn.body, newEnv);
          // } else {
          //   throw new ExecError('Trying to call non-lambda');
          // }
          // break;
          var reg1 = uniquegen();
          btcnode(node.function, reg1, btc);
          var astArguments = node.arguments
          var btcArguments = [];
          for(var i = 0; i < astArguments.length; i++){
            var treg1 = uniquegen();
            btcnode(astArguments[i], treg1, btc);
            btcArguments.push(treg1);
          }
          btc.push({'type': 'call', 'function': reg1, 'arguments': btcArguments, 'target': target});
          break;
        case "get":
          // var dict = evalExpression(node.dict, env);
          // var field = evalExpression(node.field, env);
          // if(dict.hasOwnProperty('type') || typeof(dict) != typeof({})){
          //   throw new ExecError("Trying to index non-table.");
          // }
          // return getValue(dict, padKey(field));
          // break;
          var reg1 = uniquegen();
          btcnode(node.dict, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.field, reg2, btc);
          btc.push({'type': 'get', 'dict': reg1, 'field': reg2, 'target': target});
          break;
        case "len":
          // var dict = evalExpression(node.dict, env);
          // if(dict.hasOwnProperty('type') || typeof(dict) != typeof({})){
          //   throw new ExecError("Trying to call len on non-table.");
          // } else {
          //   i = 0;
          //   for (i = 0; padKey(i) in dict; i++);
          //   return i;
          // }
          // break;
          var reg1 = uniquegen();
          btcnode(node.dict, reg1, btc);
          btc.push({'type': 'len', 'dict': reg1, 'target': target});
          break;
        case "in":
          // var element = evalExpression(node.operand1, env)
          // var dict = evalExpression(node.operand2, env);
          // if(typeof(dict) != typeof({})){
          //   throw new ExecError("Trying to find key in non-table.");
          // }
          // return makeProperBoolean(padKey(element) in dict);
          var reg1 = uniquegen();
          btcnode(node.operand1, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.operand2, reg2, btc);
          btc.push({'type': 'in', 'operand1': reg1, 'operand2': reg2, 'target': target});
          break;
        case "coroutine":
          var reg1 = uniquegen();
          btcnode(node.body, reg1, btc);
          btc.push({'type': 'coroutine', 'function': reg1, 'target': target})
          break;
        case "yield":
          var reg1 = uniquegen();
          btcnode(node.arg, reg1, btc);
          btc.push({'type': 'yield', 'value': reg1, 'target': target})
          break;
        case "resume":
          var reg1 = uniquegen();
          btcnode(node.coroutine, reg1, btc);
          var reg2 = uniquegen();
          btcnode(node.arg, reg2, btc);
          btc.push({'type': 'resume', 'coroutine': reg1, 'value': reg2, 'target': target})        
          break;
        case "exp":
          // return evalExpression(node.body, env);
          var astBody = node.body
          var btcBody = [];
          for(var i = 0; i < astBody.length; i++){
            var reg1 = uniquegen();
            btcnode(astBody[i], reg1, btcBody);
          }
          btc.push({'type': 'exp', 'body': btcBody, 'target': target});
          break;       
        /*case 'return':
          var reg1 = uniquegen();
          btcnode(node.value, reg1, btc);
          btc.push({'type': 'return', 'value': reg1, 'target': target});
          break;*/
        case 'native':
          // var func = node.function.name;
          // var args = node.arguments;

          // var jsArgs = args.map(function(n) {
          //   return toJSObject(evalExpression(n, env));
          // });
          // var jsFunc = runtime[func];

          // var ret = jsFunc.apply(null, jsArgs);
          // return to164Object(ret);
          var reg1 = uniquegen();
          btcnode(node.function, reg1, btc);
          var astArguments = node.arguments
          var btcArguments = [];
          for(var i = 0; i < astArguments.length; i++){
            var treg1 = uniquegen();
            btcnode(astArguments[i], treg1, btcArguments);
          }
          btc.push({'type': 'native', 'function': reg1, 'arguments': btcArguments, 'target': target});
          break;
        default:
          throw new Error(
            "What's " + node.type + "? " + JSON.stringify(node)
      );
      }
    }

    function btcblock(statements) {
      // TODO step 2: Complete this function so that functions have
      // explicit return statements
      var btc = [];
      var target;
      statements.forEach(function(statement, index) {
        target = uniquegen();
        btcnode(statement, target, btc, index === statements.length - 1);
      });
      if (!target) {
        // If the body of the lambda is empty, return val is null
        target = uniquegen();
        btc.push({'type': 'null', 'target': target});
      }
      btc.push({'type': 'return', 'value': target})
      return btc;
    }

    return btcblock(ast);
  }

  function to164Object(o) {
    // convert a Python object to a suitable 164 object
    var type = typeof o;
    if (type === 'number') {
      return o;
    } else if (type === 'string') {
      return o;
    } else {
      // throw new ExecError('converting unknown type')
      console.log('converting unknown type');
      return null;
    }
  }

  function toJSObject(o) {
    // convert a JS object to a suitable 164 object
    var type = typeof o;
    if (type === 'number') {
      return o;
    } else if (type === 'string') {
      return o;
    } else {
      // throw new ExecError('converting unknown type')
      console.log('converting unknown type');
      return null;
    }
  }
  
  // Returns a closure, a data structure which stores the param names
  // (id objects), the body of the function, and the referencing
  // environment, in which it was initialized --- (for lexical scoping).
  function makeClosure(names, body, env) {
    var args=[];
    if (names.length != 0) {
        for (var i = 0; i < names.length; i++) {
            args.push(names[i].name);
        };
    }
    return {
      names: names,
        body: body,
        args: args,
        type: 'closure',
        env: env,
        '\%u8__function': 1
    };
  }

  // Returns a stack frame, a data structure which stores 
  // information about the state of a function
  function makeStackFrame(bytecode, env, retReg, parentFrame) {
    // TODO step 3: decide what you need to store in a stack frame
    // based on what your bytecode interpreter needs.
    // Decide whether the arguments above are sufficient.
    //memreg
    var frame = {};
    frame["btc"] = bytecode;
    frame["env"] = env;
    frame["retReg"] = retReg;
    frame["parent"] = parentFrame;
    frame["memreg"] = {} 
    frame["pc"] = -1;
    return frame;
  }

  // Returns a new program state object, a data structure which stores
  // information about a particular stack
  function makeProgramState(bytecode, env, coroutine) { //add another argument for later parts
    // TODO step 3: decide what you need to store in a program state
    // object based on what your bytecode interpreter needs.
    // Decide whether the arguments above are sufficient.
    var state = {};
    // 0 = not a coroutine, 1 = paused coroutine, 3 = running coroutine, 2 = finished coroutine
    state["state"] = (typeof coroutine !== 'undefined') ? 1 : 0;
    state["bytecodeArray"] = bytecode;
    if (state["state"] == 1) {
        state["currentFrame"] = coroutine.currentFrame;
        state["mainState"] = coroutine.mainState;
        state["resumeReg"] = coroutine.resumeReg;
        state["prevState"] = coroutine.prevState;
        state["yieldReg"] = coroutine.yieldReg;
    } else {
        state["currentFrame"] = makeStackFrame(bytecode, env, bytecode.target);
    }
    return state;
  }

  function getValue(dict, field) {
      if (field in dict) {
          return dict[field]
      } else {
        if (typeof dict[padKey("__mt")] != "undefined" && padKey("__index") in dict[padKey("__mt")] && dict[padKey("__mt")][padKey("__index")] != dict) {
          return getValue(dict[padKey("__mt")][padKey("__index")], field)
        } else {
          throw new ExecError("Tried to get nonexistent key: " + makeString(stripKey(field)));
        }
      }
  }

  function makeProperBoolean(b){
    if(b){
      return 1;
    }
    return 0;
  }

  function stripKey(key){
    if (typeof(key) == "number" | typeof(key) == "string") {
      return key.substring(3, key.length);
    } else {
      return key;
    }
  }

  function padKey(key){
    switch (typeof(key)){
      case "number":
        return "%u9"+key;
      case "string":
        return "%u8"+key;
      default:
        return key;
    }
  }

  function makeString(toPrint) {
    if (toPrint == null | typeof toPrint == 'undefined') {
        return toPrint;
    }
    if (toPrint.type) {
        if (toPrint.type === 'closure') {
          return "Lambda";
        } else if (toPrint.type === "coroutine") {
          var costate = toPrint.coroutineState["state"];
          return "Coroutine: " + (costate == 2 ? "Dead" : costate == 3 ? "Running" : "Suspended");
        }
    } else if (typeof(toPrint) === 'object') {
        var s = "{";
        for(var key in toPrint){
          s += stripKey(key) + ": " + ((toPrint[key] == toPrint) ? "self" : makeString(toPrint[key])) + ", "; 
        }
        if (Object.keys(toPrint).length > 0){
          s = s.substring(0, s.length - 2)
        }
        s += "}"
        return s
    }
    return toPrint;
  }

  function resumeProgram(programState) {
    // TODO step 3: implement this function, which executes
    // bytecode.  See how it's called in the execBytecode function.

    function evalByteCode(node, env) {
      //T ODO: Use your own evalExpression here
      // keep the 'native' case below for using native JavaScript
      // introduce new cases as needed to implement dictionaries,
      // lists, and objects
      var memreg = programState.currentFrame.memreg
      switch (node.type) {
        //statements
        case "asgn":
          tutor164.overwriteVar(node.name, memreg[node.value], memreg[node.value] ? memreg[node.value].type ? memreg[node.value].type == 'coroutine' ? 2 : memreg[node.value].type == 'closure' : 0 : 0);
          envUpdate(env, node.name, memreg[node.value]);
          memreg[node.target] = null;
          break;
        case "def":
          tutor164.defObj(node.name, memreg[node.value], memreg[node.value] ? memreg[node.value].type ? memreg[node.value].type == 'coroutine' ? 2 : memreg[node.value].type == 'closure' : 0 : 0);
          envBind(env, node.name, memreg[node.value]);
          memreg[node.target] = null;
          break;
        case "print":
          var toPrint = memreg[node.value];
          var toPrint2 = makeString(toPrint);
          //log(toPrint2);
          tutor164.print(toPrint2);
          memreg[node.target] = null;
          break;
        case "error":
          throw new ExecError(memreg[node.message]);  //how do this?
        case "exp":
          memreg[node.target] = memreg[node.body];
          break;
        case "put":
          var dict = memreg[node.dict];
          var field = memreg[node.field];
          var value = memreg[node.value];
          if(typeof(dict) != typeof({})){
            throw new ExecError("Trying to put into non-table.");
          }
          tutor164.putValInDict(dict, field, value, value ? value.type ? value.type == 'coroutine' ? 2 : value.type == 'closure' : 0 : 0)
          dict[padKey(field)] = value
          memreg[node.target] = null;
          break;
        //expressions
        case '+':
          memreg[node.target] = memreg[node.operand1] + memreg[node.operand2];
          break;
        case '-':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to - must be numbers.');
          }
          memreg[node.target] = Math.floor(operandOne - operandTwo);
          break;
        case '*':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to * must be numbers.');
          }
          memreg[node.target] = Math.floor(operandOne * operandTwo);
          break;
        case '/':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to / must be numbers.');
          }
          if (operandTwo == 0){
            throw new ExecError('Division by zero');
          }
          memreg[node.target] = Math.floor(operandOne / operandTwo);
          break;
        case '==':
          memreg[node.target] = makeProperBoolean(memreg[node.operand1] == memreg[node.operand2]);
          break;
        case '!=':
          memreg[node.target] = makeProperBoolean(memreg[node.operand1] != memreg[node.operand2]);
          break;
        case '<=':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to <= must be numbers.');
          }
          memreg[node.target] = makeProperBoolean(operandOne <= operandTwo);
          break;
        case '>=':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to >= must be numbers.');
          }
          memreg[node.target] = makeProperBoolean(operandOne >= operandTwo);
          break;
        case '<':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to < must be numbers.');
          }
          memreg[node.target] = makeProperBoolean(operandOne < operandTwo);
          break;
        case '>':
          var operandOne = memreg[node.operand1];
          var operandTwo = memreg[node.operand2];
          if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
            throw new ExecError('Operands to > must be numbers.');
          }
          memreg[node.target] = makeProperBoolean(operandOne > operandTwo);
          break;
        case "id":
          memreg[node.target] = envLookup(env, node.name);
          break;
        case "int-lit":
          memreg[node.target] = node.value;
          break;
        case "string-lit":
          memreg[node.target] = node.value;
          break;
        case "null":
          memreg[node.target] = null;
          break;
        case "ite":
          var cond = memreg[node.condition];
          var ct = memreg[node.true];
          var cf = memreg[node.false];
          if (cond == null) {
            cond = false;
          }
          if ((typeof cond != 'boolean') && (typeof cond != 'number')) {
            throw new ExecError('Condition not a boolean');
          }
          memreg[node.target] = cond ? ct : cf;
          break;
        case "lambda":
          memreg[node.target] = makeClosure(node.arguments, node.body, env);
          break;
        case "empty-dict-lit":
          memreg[node.target] = {};
          break;
        case "tcall":
        case "call":
          var fn = memreg[node.function];
          if (fn != null && fn.type && fn.type === 'closure') {
            if (fn.names.length != node.arguments.length){
              throw new ExecError('Wrong number of arguments');
            } 
            var newFrame
            if (node.type == "tcall") {
                 newFrame = makeStackFrame(fn.body, envExtend(fn.env), programState["currentFrame"]["retReg"],
                    programState["currentFrame"]["parent"]);
            } else {
                 newFrame = makeStackFrame(fn.body, envExtend(fn.env), node.target, programState["currentFrame"]);
            }
            var args = [];
            for (var i = 0; i < node.arguments.length; i++) {
              args.push([memreg[node.arguments[i]], memreg[node.arguments[i]] ? memreg[node.arguments[i]].type ? memreg[node.arguments[i]].type == 'coroutine' ? 2 : memreg[node.arguments[i]].type == 'closure' : 0 : 0]);
              envBind(newFrame["env"], fn.names[i].name, memreg[node.arguments[i]]);
            }
            if (node.type == "tcall") {
                tutor164.callFunc(fn, args);
            } else {
                tutor164.callFunc(fn, args);
            }
            programState["currentFrame"] = newFrame;
          } else {
            throw new ExecError('Trying to call non-lambda');
          }
          break;
        case "get":
          var dict = memreg[node.dict];
          var field = memreg[node.field];
          if(dict.hasOwnProperty('type') || typeof(dict) != typeof({})){
            throw new ExecError("Trying to index non-table.");
          }
          memreg[node.target] = getValue(dict, padKey(field));
          break;
        case "len":
          var dict = memreg[node.dict];
          if(dict.hasOwnProperty('type') || typeof(dict) != typeof({})){
            throw new ExecError("Trying to call len on non-table.");
          } else {
            i = 0;
            for (i = 0; padKey(i) in dict; i++);
            memreg[node.target] = i;
          }
          break;
        case "in":
          var element = memreg[node.operand1];
          var dict = memreg[node.operand2];
          if(typeof(dict) != typeof({})){
            throw new ExecError("Trying to find key in non-table.");
          }
          memreg[node.target] = makeProperBoolean(padKey(element) in dict);
          break;
        case "coroutine":
          var coroutine = {}
          var fn = memreg[node.function];
          if (fn != null && fn.type && fn.type === 'closure') {
            if (fn.names.length != 1){
              throw new ExecError('Coroutine lambdas must accept exactly one argument.');
            } 
            var newFrame = makeStackFrame(fn.body, envExtend(fn.env), node.target, undefined);
          } else {
            throw new ExecError('Tried to make a coroutine of a non-lambda');
          }
          coroutine["type"] = "coroutine";
          coroutine["fn"] = fn;
          coroutine["mainState"] = programState;
          coroutine["currentFrame"] = newFrame;
          coroutine["coroutineState"] = makeProgramState(newFrame.btc, env, coroutine);
          memreg[node.target] = coroutine;
          break;
        case "yield":
          if (programState["state"] % 2 == 0) {
              throw new ExecError("Tried to yield from non-coroutine.");
          } else if (programState["state"] != 3) {
              throw new ExecError("Tried to yield from paused coroutine.");
          }
          programState.yieldReg = node.target;
          programState.state = 1;
          programState.prevState.currentFrame.memreg[programState.resumeReg] = memreg[node.value];
          tutor164.yieldCo(memreg[node.value], memreg[node.value] ? memreg[node.value].type ? memreg[node.value].type == 'coroutine' ? 2 : memreg[node.value].type == 'closure' : 0 : 0);
          programState = programState.prevState;
          break;
        case "resume":
          var coroutine = memreg[node.coroutine];
          if (typeof coroutine === 'undefined' | typeof coroutine.coroutineState === 'undefined') {
              throw new ExecError("Tried to call resume on non-coroutine.");
          }
          if (coroutine.coroutineState["state"] != 1) {
              throw new ExecError("Coroutine not resumable.");
          }
          coroutine.coroutineState.resumeReg = node.target;
          if (typeof coroutine.coroutineState.yieldReg !== 'undefined') {
            coroutine.coroutineState.currentFrame.memreg[coroutine.coroutineState.yieldReg] = memreg[node.value];
          } else {
            envBind(coroutine.currentFrame["env"], coroutine.fn.names[0].name, memreg[node.value]);
          }
          coroutine.coroutineState["state"] = 3;
          coroutine.coroutineState.prevState = programState;
          tutor164.resumeCo(coroutine, memreg[node.value], memreg[node.value] ? memreg[node.value].type ? memreg[node.value].type == 'closure' ? 1 : memreg[node.value].type == 'coroutine' : 0 : 0);
          programState = coroutine.coroutineState;
          break;
        case "exp":
          memreg[node.target] = memreg[node.body];
          break;
        case 'native':
          var func = node.function.name;
          var args = node.arguments;

          var jsArgs = args.map(function(n) {
            return toJSObject(memreg[n]);
          });
          var jsFunc = runtime[func];

          var ret = jsFunc.apply(null, jsArgs);
          memreg[node.target] = to164Object(ret);
          break;
        case "return":
          tutor164.functReturn(memreg[node.value], memreg[node.value] ? memreg[node.value].type ? memreg[node.value].type == 'coroutine' ? 2 : memreg[node.value].type == 'closure' : 0 : 0);
          if (typeof programState.currentFrame["parent"] !== 'undefined') {
            programState.currentFrame["parent"].memreg[programState["currentFrame"]["retReg"]] = memreg[node.value];
            programState["currentFrame"] = programState["currentFrame"]["parent"]
          } else {
            if (programState["state"] == 3) {
              programState.mainState.currentFrame.memreg[programState.resumeReg] = memreg[node.value];
              programState["state"] = 2;
              programState = programState.mainState;
            } else {
              programState["currentFrame"] = undefined
            }
          }
          break;
        default:
          throw new Error(
            "What's " + node.type + "? " + JSON.stringify(node)
        );
      }
    }

    while (true) {
        programState["currentFrame"]["pc"] += 1;
        tutor164.nextLine();
        if (programState["currentFrame"]["pc"] >= programState["currentFrame"]["btc"].length) {
            throw new ExecError("Ran out of code to execute");
        }
        evalByteCode(programState["currentFrame"]["btc"][programState["currentFrame"]["pc"]], programState["currentFrame"]["env"]);
        if ((typeof programState["currentFrame"]) === 'undefined') {
            return;
        }
    }
  }

  function execBytecode(bytecode, env) {
    // TODO step 3: based on how you decide to implement
    // makeProgramState, make sure the makeProgramState call below
    // suits your purposes.
    return resumeProgram(makeProgramState(bytecode, env));
  }

  function tailCallOptimization(insts){
    // TODO step 5: implement this function, (which is called below).
    // It should take bytecode as input and transform call instructions
    // into tcall instructions if they can be optimized with the
    // tail call optimization.
    var inst;
    var memreg = {};
    for (inst in insts) {
        memreg[insts[inst].target] = inst;
        if (insts[inst].type == "return" && insts[memreg[insts[inst].value]].type == "call") {
            insts[memreg[insts[inst].value]].type = "tcall";
        } else if (insts[inst].type == "lambda") {
            tailCallOptimization(insts[inst].body);
        }
    }
  }

  function desugarAll(remaining, desugared, callback) {
    if (remaining.length == 0) {
      setTimeout(function () {
        callback(desugared);
      }, 0);
      return;
    }

    var head = remaining.shift();
    desugarAST(head, function(desugaredAst) {
      desugared.push(desugaredAst);
      desugarAll(remaining, desugared, callback);
    });
  }
  
  var root = envRoot();
  root['*title'] = 'Root';
  envBind(root,"type", function(a){
    if (a && a.type==="table"){
      return "table";
    } else {
      return "other";
    }
  });

  desugarAll(asts, [], function(desugaredAsts) {
    for (var i = 0, ii = desugaredAsts.length; i < ii; ++i) {
      try {
        tutor164.resetTrace();
        tutor164.moveLine(0);
        var bytecode = compileToBytecode(desugaredAsts[i]);
        //tailCallOptimization(bytecode);
        execBytecode(bytecode, root);
      } catch (e) {
        tutor164.throwError(e.message);
        if (e instanceof ExecError) {
          //log('Error: ' + e.message);
        } else {
          throw e;
        }
      }
      log(tutor164.trace);
    }
  });
  return tutor164.trace
};

if (typeof(module) !== 'undefined') {
  module.exports = {
    'interpret': interpret
  };
}
