function Tutor() {
    this.state =
        {
             "ordered_globals": [], 
             "stdout": "", 
             "func_name": "<module>", 
             "stack_to_render": [], 
             "globals": {}, 
             "heap": {}, 
             "line": 0, 
             "event": "step_line"
        };
    this.trace = [];
    this.heapCount = 1;
    this.curStack = [];
    this.stackCount = 1;
    this.curFrameId = null;
    this.metaChar;
    this.returnLines = [];
}

Tutor.prototype = {
    nextLine: function() {
        if (this.getCurFrameId() == null) {
            this.state["line"] += 1;
        }
        this.state["event"] = "step_line"
        this.saveState();
    },
    moveLine: function(ln) {
        this.state["line"] = ln;
    },
    throwError: function(error) {
        this.state["event"] = "exception";
        this.state["exception_msg"] = error;
        this.saveState();
    },
    print: function(output) {
        this.state["stdout"] += output + "\n";
    },
    saveState: function() {
        this.trace.push(JSON.parse(JSON.stringify(this.state)));
    },
    getStackFrame: function(id) {
        for (var i = 0; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["frame_id"] == id) {
                return this.state["stack_to_render"][i];
            }
        }
        return null;
    },
    getCurFrameId: function() {
        var i = 0;
        for (i; i < this.state["stack_to_render"].length; i++) {
            if (this.state["stack_to_render"][i]["is_highlighted"]) {
                return this.state["stack_to_render"][i]["frame_id"];
            }
        }
        return null;
    },
    functReturn: function(val, type) {
        this.state["event"] = "return";
        var i = this.getCurFrameId()
        var cframe = this.getStackFrame(i);
        if (i == null) {
            this.saveState()
            return
        }
        this.defObj("__return__", val, type)
        this.saveState()
        if (!cframe.is_parent){
            this.state["stack_to_render"].pop(this.state["stack_to_render"].indexOf(cframe));
        } else {
            cframe["is_highlighted"] = false;
            cframe.is_zombie = true;
            cframe["unique_hash"] += "_z";
        }
        if (this.curStack.length == 0) {
            this.state["func_name"] = "<module>";
        } else {
            var tstack = this.getStackFrame(this.curStack.pop());
            tstack["is_highlighted"] = true;
            this.state["func_name"] = tstack["func_name"];
        }
        this.moveLine(this.returnLines.pop());
    },
    defVal: function(input) {
        if (typeof(input) != 'undefined' && input != null && typeof(input) == typeof({})) {
            var tmp;
            if ("\%uid" in input) {
                tmp = input["\%uid"];
            } else {
                tmp = this.heapCount++;
                input["\%uid"] = tmp;
                this.state["heap"][tmp.toString()] = ["DICT"];
            }
            return ["REF", tmp];
        } else {
            return input;
        }
    },
    defObj: function(name, val, type) {
        name = this.cleanName(name);
        var cframeId = this.getCurFrameId();
        if (cframeId == null) {
            this.state["ordered_globals"].push(name);
            this.state["globals"][name] = this.evalInput(val, type, name);
        } else {
            var tframe = this.getStackFrame(cframeId);
            tframe["ordered_varnames"].push(name);
            tframe["encoded_locals"][name] = this.evalInput(val, type, name);
        }
    },
    getHeapVal: function(ref) {
        return this.state["heap"][ref[1]];
    },
    evalInput: function(val, type, name) {
        if (type && (typeof(val["\%uid"]) == 'undefined')) {
            return this.createFunc(val, name);
        } else {
            return this.defVal(val);
        }
    },
    putValInDict: function(dict, key, inVal, type) {
        var dval = this.evalInput(dict, false, "");
        var heapDict = this.getHeapVal(dval);
        var val = this.evalInput(inVal, type, key);
        var i = 1;
        for (i; i < heapDict.length; i++) {
            if (heapDict[i] == key) {
                heapDict[i][1] = val;
                return;
            }
        }
        heapDict.push([key, val]);
    },
    overwriteVar: function(varName, inVal, type) {
        varName = this.cleanName(varName);
        var val = this.evalInput(inVal, type, varName);
        var tFrame = this.getStackFrame(this.getCurFrameId());
        if (tFrame != null) {
            if (tFrame["ordered_varnames"].indexOf(varName) != -1) {
                return tFrame["encoded_locals"][varName] = val;
            }
            var i = 0;
            var found = false;
            var ttFrame;
            for (i; i < tFrame["parent_frame_id_list"].length; i++) {
                ttFrame = this.getStackFrame(tFrame["parent_frame_id_list"][i]);
                if (ttFrame["ordered_varnames"].indexOf(varName) != -1) {
                    return ttFrame["encoded_locals"][varName] = val;
                }
            }
        }
        if (this.state["ordered_globals"].indexOf(varName) != -1) {
            return this.state["globals"][varName] = val;
        }
        throw "Variable " + varName + " can't be found";
    },
    createFunc: function(val, name) {
        if ("\%uid" in val) {
            return ["REF", val["\%uid"]];
        }
        var cframeId = this.getCurFrameId();
        if (cframeId != null) {
            var cframe = this.getStackFrame(cframeId);
            cframe["is_parent"] = true;
            cframe["unique_hash"] += "_p";
        }
        tmp = this.heapCount++;
        val["\%uid"] = tmp;
        var sig = name + '(' + val["args"].join(", ") + ')';
        this.state["heap"][tmp.toString()] = ["FUNCTION", sig, cframeId, this.state["line"]];
        return ["REF", tmp];
    },
    callFunc: function(val, arg_vals) {
        if (typeof(val) == 'undefined') {
            return
        }
        var cframeId = this.getCurFrameId();
        if (cframeId != null) {
            var cframe = this.getStackFrame(cframeId);
            cframe["is_highlighted"] = false;
            this.curStack.push(cframeId);
        }
        var isLambda = !("\%uid" in val);
        var id = this.stackCount++;
        var name = isLambda ? "lambda" : this.state["heap"][val["\%uid"].toString()][1];
        var newStack = 
        {
           "frame_id": id, 
           "encoded_locals": {}, 
           "is_highlighted": true, 
           "is_parent": false, 
           "func_name": name, 
           "is_zombie": false, 
           "parent_frame_id_list": [], 
           "unique_hash": name + "_f" + id, 
           "ordered_varnames": [],
           "stack_num": 0
        }
        if (!isLambda) {
            var parentId = this.state["heap"][val["\%uid"].toString()][2];
            if (parentId != null) {
                var pStack = this.getStackFrame(parentId);
                newStack["parent_frame_id_list"].push(parentId);
                newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(pStack["parent_frame_id_list"]);
            }
        } else if (cframeId != null) {
            newStack["parent_frame_id_list"].push(cframeId);
            newStack["parent_frame_id_list"] = newStack["parent_frame_id_list"].concat(cframe["parent_frame_id_list"]);
        }
        this.state["stack_to_render"].push(newStack);
        var arglength = arg_vals.length > val["args"].length ? val["args"].length : arg_vals.length;
        for (var i = 0; i < arglength; i++) {
            this.defObj(val["args"][i], arg_vals[i][0], arg_vals[i][1]);
        }
        this.returnLines.push(this.state["line"]);
        if (!isLambda) {
            this.moveLine(this.state["heap"][val["\%uid"].toString()][3]);
        }
        this.state["event"] = "call";
        //this.saveState();
    },
    cleanName: function(name) {
        return name.replace('#','--');
    }
}

var Tutor164 = Tutor;

if (typeof(module) !== 'undefined') {
  module.exports = {
    'Tutor164': Tutor164
  };
}
        


    


