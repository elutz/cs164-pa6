if (typeof(module) !== 'undefined') {
  var ExecError = require('./errors.js').ExecError;
  var desugarAST = require('./desugar.js').desugarAST;
  var Tutor164 = require('./tutor164.js').Tutor164;
  var env = require('./environment.js');
  var envRoot = env.root;
  var envExtend = env.extend;
  var envBind = env.bind;
  var envUpdate = env.update;
  var envLookup = env.lookup;
}

var tutor164 = new Tutor164();

var interpret = function(asts, log, err) {
  console.log(asts)

  var root = envRoot();
  root['*title'] = 'Root';

  // TODO: Complete the closure implementation. What's missing?
  function makeClosure(names, body, env) {
    var args=[];
    if (names.length != 0) {
        for (var i = 0; i < names.length; i++) {
            args.push(names[i].name);
        };
    }
    return {
      names: names,
        body: body,
        type: 'closure',
        env: env,
        args: args
    };
  }

  function evalBlock(t, env) {
    var last = null;
    console.log(t)
    console.log(t.forEach)
    t.forEach(function(n) {
      tutor164.nextLine();
      last = evalStatement(n, env);
    });
    tutor164.functReturn(last, last ? last.type ? last.type == 'closure': false : false);
    return last;
  }
  
  function evalExpression(node, env){
    //TODO: Implement the expressions that aren't yet handled.
    switch (node.type){
      case '+':
        return evalExpression(node.operand1, env) + evalExpression(node.operand2, env);
      case '-':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to - must be numbers.');
        }
        return Math.floor(operandOne - operandTwo);
      case '*':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to * must be numbers.');
        }
        return Math.floor(operandOne * operandTwo);
      case '/':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to / must be numbers.');
        }
        if (operandTwo == 0){
          throw new ExecError('Division by zero');
        }
        return Math.floor(operandOne / operandTwo);
      case '==':
        return makeProperBoolean(evalExpression(node.operand1, env) == evalExpression(node.operand2, env));
      case '!=':
        return makeProperBoolean(evalExpression(node.operand1, env) != evalExpression(node.operand2, env));
      case '<=':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to <= must be numbers.');
        }
        return makeProperBoolean(operandOne <= operandTwo);
      case '>=':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to >= must be numbers.');
        }
        return makeProperBoolean(operandOne >= operandTwo);
      case '<':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to < must be numbers.');
        }
        return makeProperBoolean(operandOne < operandTwo);
      case '>':
        var operandOne = evalExpression(node.operand1, env);
        var operandTwo = evalExpression(node.operand2, env);
        if (operandOne==null || (typeof(operandOne) != 'number') || operandTwo==null || (typeof(operandTwo) != 'number')){
          throw new ExecError('Operands to > must be numbers.');
        }
        return makeProperBoolean(operandOne > operandTwo);
      case "id":
        return envLookup(env, node.name);
      case "int-lit":
        return node.value;
      case "string-lit":
        return node.value;
      case "null":
        return null;
      case "ite":
        var cond = evalExpression(node.condition, env);
        var ct = evalExpression(node.true,  env);
        var cf = evalExpression(node.false, env);
        if (cond == null) {
          cond = false;
        }
        if ((typeof cond != 'boolean') && (typeof cond != 'number')) {
          throw new ExecError('Condition not a boolean');
        }
        return cond ? ct : cf;
      case "lambda":
        return makeClosure(node.arguments, node.body, env);
      case "call":
        var fn = evalExpression(node.function, env);
        if (fn != null && fn.type && fn.type === 'closure') {
          if (fn.names.length != node.arguments.length){
            throw new ExecError('Wrong number of arguments');
          }
          var newEnv = envExtend(fn.env);
          var args = [];
          var t;
          for (i = 0; i < node.arguments.length; i++) {
            t = evalExpression(node.arguments[i], env)
            args.push([t, t ? t.type ? t.type == "closure" : false : false]);
            envBind(newEnv, fn.names[i].name,t);
          }
          tutor164.callFunc(fn, args);
          return evalBlock(fn.body, newEnv);
        } else {
          throw new ExecError('Trying to call non-lambda');
        }
        break;
      case "dict-lit":
        var dict = {}
        /*for (i = 0; i < node.value.length; i++) {
          dict[node.value[i].name] = evalExpression(node.value[i].value, env);
        }*/
        return dict;
      case "get":
        var dict = evalExpression(node.dict, env);
        var field = evalExpression(node.field, env);
        if(typeof(dict) != typeof({})){
          throw new ExecError("dictionary is not a dictionary"); //Check exact wording this is supposed to be
        }
        if (field in dict){
          return dict[field]
        } else {
          throw new ExecError("dictionary does not contain value " + field); //check exact wording this is suposed to be
        }
        break;
      case "len":
        var dict = evalExpression(node.dict, env);
        if(typeof(dict) != typeof({})){
          throw new ExecError("dictionary is not a dictionary"); //Check exact wording this is supposed to be
        } else {
          i = 0;
          for (i = 0; i in dict; i++);
          return i;
        }
        break;
      case "in":
        var element = evalExpression(node.operand1, env)
        var dict = evalExpression(node.operand2, env);
        if(typeof(dict) != typeof({})){
          throw new ExecError("operand2 is not a dictionary"); //Check exact wording this is supposed to be
        }
        if(typeof(element) == typeof(nil)){
          throw new ExecError("element " + element + " does not exist"); //Check exact wording this is supposed to be
        }
        return makeProperBoolean(element in dict);
      case "exp":
        return evalExpression(node.body, env);
      default:
        throw new Error(
          "What's " + node.type + "? " + JSON.stringify(node)
      );
      
    }
  }

  function makeProperBoolean(b){
    if(b){
      return 1;
    }
    return 0;
  }

  function evalStatement(node, env) {
    switch (node.type) {
      // TODO: Complete for statements that aren't handled
      case "asgn":
        var value = evalExpression(node.value, env);
        envUpdate(env, node.name.name,value);
        tutor164.overwriteVar(node.name.name, value, value ? value.type ? value.type == "closure" : false : false);
        return null;
      case "def":
        var value = evalExpression(node.value, env);
        envBind(env, node.name.name, value);
        tutor164.defObj(node.name.name, value, value ? value.type ? value.type == "closure" : false : false);
        return null;
      case "print":
        var toPrint = evalExpression(node.value, env)
        if (toPrint != null && toPrint.type && toPrint.type === 'closure') {
          log("Lambda")
          tutor164.print("Lambda");
        } else {
          log(toPrint);
          tutor164.print(toPrint);
        }
        return null;
      case "error":
        var msg = evalExpression(node.message, env)
        throw new ExecError(msg);
      case "exp":
        return evalExpression(node.body, env);
      case "put":
        var dict = evalExpression(node.dict, env);
        var field = evalExpression(node.field, env);
        var value = evalExpression(node.value, env);
        if(typeof(dict) != typeof({})){
          throw new ExecError("dictionary is not a dictionary"); //Check exact wording this is supposed to be
        }
        dict[field] = value
        tutor164.putValInDict(dict, field, value, value ? value.type ? value.type == "closure" : false : false);
        break;
        
      default:
        throw new Error(
          "What's " + node.type + "? " + JSON.stringify(node)
      );
    }
  }

  var desugarOne = function(asts){
    var ast, remaining_asts;
    if (asts.length > 0){
      ast = asts.shift(); //pops first item
      remaining_asts = asts; //first item already popped
    }
    else{ return; } // no more ASTs to eval
    desugarAST(ast, function(ast) {
      try {
        evalBlock(ast, root);
        desugarOne(remaining_asts);
        log(JSON.stringify(tutor164.trace));
      } catch (e) {
        tutor164.throwError(e.message);
        log(JSON.stringify(tutor164.trace));
        if (e.constructor === ExecError) {
          log('Error: ' + e.message);
        } else {
          throw e;
        }
      }
    });
  };
  desugarOne(asts);
  return tutor164.trace;
};

// Makes the interpreter importable in Node.js
if (typeof(module) !== 'undefined') {
  module.exports = {
    'interpret': interpret
  };
}
